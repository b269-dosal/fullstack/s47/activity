const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

// ACTIVITY S47

// [METHOD 1]

 txtFirstName.addEventListener("keyup", (event) => {
  const fullName = txtFirstName.value + " " + txtLastName.value;
  spanFullName.innerHTML = fullName; 
  console.log(event.target);
  console.log(event.target.value);
});

txtLastName.addEventListener("keyup", (mangyayari) => {
  const fullName = txtFirstName.value + " " + txtLastName.value;
  spanFullName.innerHTML = fullName; 
  console.log(mangyayari.target);
  console.log(mangyayari.target.value);
});
 

// [METHOD 2]
/* function updateFullName() {
  const fullName = txtFirstName.value + " " + txtLastName.value;
  spanFullName.innerHTML = fullName; 
}

txtFirstName.addEventListener("input", updateFullName);
txtLastName.addEventListener("input", updateFullName);
 */

// [METHOD 3]
/* function updateFullName() {
  const fullName = `${txtFirstName.value} ${txtLastName.value}`;
  spanFullName.textContent = fullName; 
  console.log(`${txtFirstName.value} ${txtLastName.value}`);
}

txtFirstName.addEventListener("input", updateFullName);
txtLastName.addEventListener("input", updateFullName);
 */